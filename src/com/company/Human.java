package com.company;

import java.util.HashSet;
import java.util.Set;

public class Human implements Subject {
    private Set<Observer> observers;
    private String name;
    private long phoneNumber;
    private Human marriedOn;
    private boolean isWorkingInHR;

    Human() {
        observers = new HashSet<>();
    }

    @Override
    public void registerObserver(Observer o) {
        if (observers.add(o)) {
            System.out.println(name + ": Registered the Observer: " + o.getClass().getSimpleName() + "!!!");
        } else {
            System.out.println(o.getClass().getSimpleName() + " already registered on " + name + "!!!");
        }
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObserversChangePhone() {
        for (Object i : observers) {
            Observer observer = (Observer) i;
            observer.updatePhoneNumber(getEmployee(), phoneNumber);
        }
    }

    private Human getEmployee() {
        Human employee;
        if (isWorkingInHR) {
            employee = this;
        } else {
            employee = getMarriedOn();
        }
        return employee;
    }

    @Override
    public void notifyObserversMarried() {
        for (Object collection : observers) {
            Observer observer = (Observer) collection;
            observer.marriedOn(getMarriedOn());
        }
    }

    String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    void setPhoneNumber(long phoneNumber) {
        this.phoneNumber = phoneNumber;

        // If you remove the check for! IsWorkingInHR, then we will receive notifications about changing phones not only
        // spouse of the employee, but also of the employee himself.
        if (phoneNumber != 0 && !isWorkingInHR) {
            notifyObserversChangePhone();
        }
    }

    Human getMarriedOn() {
        return marriedOn;
    }

    void setMarriedOn(Human marriedOn) {
        this.marriedOn = marriedOn;
        notifyObserversMarried();
    }

    void setWorkingInHR() {
        isWorkingInHR = true;
    }
}