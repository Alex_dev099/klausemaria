package com.company;

public abstract class HumanObserver implements Observer {

    @Override
    public void checkMaritalStatusAndSubscribe(Human human) {
        if (human.getMarriedOn() != null) {
            Subject spouse = human.getMarriedOn();
            spouse.registerObserver(this);
        } else {
            human.registerObserver(this);
        }
    }

    @Override
    public void marriedOn(Human spouse) {
        spouse.registerObserver(this);
    }

    @Override
    public void updatePhoneNumber(Human employee, long phoneNumber) {
        System.out.println("Hi everyone! My wife " + employee.getMarriedOn().getName() + " has change the phone number!!! ("
                + employee.getName() + ")");
    }
}