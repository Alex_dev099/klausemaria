package com.company;

import java.util.ArrayList;

public class GrandMother extends HumanObserver {
    private ArrayList<Human> family = new ArrayList<>();

    @Override
    public void updatePhoneNumber(Human employee, long phoneNumber) {
        System.out.println("GrandMother found out about the new phone grandson's wife!!!");
    }

    void addFamilyMember(Human human) {
        family.add(human);
        checkMaritalStatusAndSubscribe(human);
    }

    public ArrayList<Human> getFamily() {
        return family;
    }
}