package com.company;

import java.util.ArrayList;

class HR extends HumanObserver {
    private ArrayList<Human> employers = new ArrayList<>();

    @Override
    public void updatePhoneNumber(Human employee, long phoneNumber) {
        System.out.println(employee.getName() + ":{новый телефон семьи}: " + phoneNumber);
    }

    ArrayList<Human> getEmployers() {
        return employers;
    }

    void addEmployee(Human human) {
        employers.add(human);
        human.setWorkingInHR();
        checkMaritalStatusAndSubscribe(human);
    }
}