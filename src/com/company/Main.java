package com.company;

public class Main {

    public static void main(String[] args) {
        // write your code here

        Human Klaus = new Human();
        Klaus.setName("Клаус");
        Human Maria = new Human();
        Maria.setName("Мария");
        HR hr = new HR();
        GrandMother grandMother = new GrandMother();

        hr.addEmployee(Klaus);
//        grandMother.addFamilyMember(Klaus);


        WeddingAgency weddingAgency = new WeddingAgency();
        weddingAgency.weddingHumans(Klaus, Maria);

        System.out.println(Klaus.getName() + " is married on " + Klaus.getMarriedOn().getName());
        System.out.println(Maria.getName() + " is married on " + Maria.getMarriedOn().getName());

        grandMother.addFamilyMember(Klaus);

        Klaus.setPhoneNumber(56454);
        Maria.setPhoneNumber(12345);
        Maria.setPhoneNumber(5555);
        Klaus.setPhoneNumber(2222);
    }
}
