package com.company;

public interface Observer {
    void updatePhoneNumber(Human employee, long phoneNumber);

    void marriedOn(Human spouse);

    void checkMaritalStatusAndSubscribe(Human human);
}